export class CalculatePriceFinancier {
  #iy = 0;
  #PV = 0;
  #ny = 0;

  constructor(iy, PV, ny) {
    this.#iy = iy;
    this.#PV = PV;
    this.#ny = ny;
  }

  static calcpt(iy, PV, ny) {
    const i =
      parseFloat(((Math.pow(1 + iy, 1 / 12) - 1) * 100).toFixed(4)) / 100;
    const n = ny * 12;
    const P = ((Math.pow(i + 1, n) * i) / (Math.pow(i + 1, n) - 1)) * PV;
    const j = PV * i;
    const a = P - j;

    return {
      PV,
      n,
      j,
      i,
      P,
      a,
    };
  }

  calculate() {
    const result = [];
    let auxPV = this.#PV;
    let auxn = this.#ny * 12;
    for (let pt = 1; pt <= this.#ny * 12; pt++) {
      const { n, P, j, a } = CalculatePriceFinancier.calcpt(
        this.#iy,
        auxPV,
        auxn / 12
      );

      result.push({
        P: P.toFixed(2),
        j: j.toFixed(2),
        a: a.toFixed(2),
      });

      auxPV -= a;
      auxn -= 1;
    }

    return result;
  }
}
