<img src="https://pbs.twimg.com/profile_images/1285630920263966721/Uk6O1QGC_400x400.jpg" align="right" width="50px">

<br><br>

# Real Estate Finance - Pacote NPM

Este é um arquivo README para a biblioteca desenvolvida para calcular financiamento imobiliário.

## Visão Geral

Ao comprar um imóvel, desejamos ter previsibilidade de quanto vamos pagar por mês, e como podemos amortizar o valor, diminuindo o valor das parcelas e o prazo de pagamento.

## Características

- **Cálculo na Tabela Price**: A biblioteca contém uma classe que, com alguns parâmetros é possível fazer o cálculo da tabela Price do seu financiamento.

### Pré-requisitos

Certifique-se de ter as seguintes ferramentas instaladas em sua máquina.

- Node.js;
- NPM.

## Instalação

Para instalar esta biblioteca em um projeto, você pode usar o gerenciador de pacotes NPM. Execute o seguinte comando no diretório raiz do seu projeto:

```bash
npm install @brunomp51/real-estate-finance
```

## Contribuição

Contribuições são bem-vindas! Se você quiser adicionar novas funcionalidades ou melhorar as existentes, sinta-se à vontade para enviar um pull request para o repositório da biblioteca no GitLab. Certifique-se de seguir as diretrizes de contribuição e de teste fornecidas no projeto.

## Licença

Esta biblioteca está licenciada sob a **Licença MIT**. Sinta-se à vontade para usá-la em seus projetos comerciais ou pessoais.

<br>

<img src="https://pbs.twimg.com/profile_images/1285630920263966721/Uk6O1QGC_400x400.jpg" width="50px">
